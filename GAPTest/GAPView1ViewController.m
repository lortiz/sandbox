//
//  GAPView1ViewController.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPView1ViewController.h"
#import "GAPSingleton.h"

@interface GAPView1ViewController ()

@property (nonatomic,assign) IBOutlet UILabel *message;
-(void)showMessage;
@end

@implementation GAPView1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [self showMessage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UI Configuration

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark Core Functions

-(void)showMessage{
    GAPSingleton *singleton = [GAPSingleton sharedInstance];
    self.message.text = singleton.userInput;
}


@end

//
//  GAPAppDelegate.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPAppDelegate.h"
#import "GAPView1ViewController.h"

#import "GAPView1ViewController.h"
#import "GAPView2ViewController.h"

@interface GAPAppDelegate()
-(void)setNavigationRoot;
@end
@implementation GAPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    UIViewController *viewController1 = [[[GAPView1ViewController alloc] initWithNibName:@"GAPView1ViewController" bundle:nil] autorelease];
    self.window.rootViewController = viewController1;
    [self.window makeKeyAndVisible];
     */
    [self setNavigationRoot];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Navigation
-(void)setNavigationRoot{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    GAPView1ViewController *vc1 = [[[GAPView1ViewController alloc]initWithNibName:@"GAPView1ViewController" bundle:nil]autorelease];
    vc1.title = NSLocalizedString(@"VIEW1_TITLE", @"");
    GAPView2ViewController *vc2 = [[[GAPView2ViewController alloc]initWithNibName:@"GAPView2ViewController" bundle:nil]autorelease];
    vc2.title = NSLocalizedString(@"VIEW2_TITLE", @"");
    UITabBarController *tbController = [[UITabBarController alloc]init];
    tbController.viewControllers = [NSArray arrayWithObjects:vc1,vc2,nil];
    [[UITabBar appearance] setTintColor:[UIColor redColor]];
    [self.window addSubview:tbController.view];
    
    
    
}
@end

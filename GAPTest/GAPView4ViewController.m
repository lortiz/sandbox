//
//  GAPView4ViewController.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPView4ViewController.h"
#import "GAPSingleton.h"

@interface GAPView4ViewController ()
@property (nonatomic,assign) IBOutlet UITextField *textField;
@end

@implementation GAPView4ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark UI Configuration

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark Actions

-(IBAction)dismissClicked:(id)sender{
    GAPSingleton *singleton = [GAPSingleton sharedInstance];
    singleton.userInput = self.textField.text;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UITextfield delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


@end

//
//  GAPSingleton.h
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//


/* 
 This is a singleton class used to store the input from the user
 */
#import <Foundation/Foundation.h>

@interface GAPSingleton : NSObject{}

@property (nonatomic,strong) NSString *userInput;
+(GAPSingleton *) sharedInstance;

@end

//
//  GAPView2ViewController.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPView2ViewController.h"
#import "GAPView3ViewController.h"

@interface GAPView2ViewController ()

@end

@implementation GAPView2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UI Configuration

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark Actions

-(IBAction)buttomClicked:(id)sender{
    GAPView3ViewController *vc3 = [[[GAPView3ViewController alloc]initWithNibName:@"GAPView3ViewController" bundle:nil]autorelease];
    [self presentViewController:vc3 animated:YES completion:nil];
}

@end

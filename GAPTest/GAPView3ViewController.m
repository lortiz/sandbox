//
//  GAPView3ViewController.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPView3ViewController.h"
#import "GAPView4ViewController.h"

@interface GAPView3ViewController ()

@end

@implementation GAPView3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToView2:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark -
#pragma mark UI Configuration

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
#pragma mark -
#pragma mark Actions

-(IBAction)goToView4:(id)sender{
    GAPView4ViewController *vc4 = [[[GAPView4ViewController alloc]initWithNibName:@"GAPView4ViewController" bundle:nil]autorelease];
    [self presentViewController:vc4 animated:YES completion:nil];
}

@end

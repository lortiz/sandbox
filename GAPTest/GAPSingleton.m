//
//  GAPSingleton.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import "GAPSingleton.h"

static GAPSingleton *_sharedInstance;

@implementation GAPSingleton

+(GAPSingleton *) sharedInstance{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedInstance = [[self alloc]init];
    });
    return _sharedInstance;
}

@end

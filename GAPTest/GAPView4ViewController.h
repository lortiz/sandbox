//
//  GAPView4ViewController.h
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAPView4ViewController : UIViewController<UITextFieldDelegate>

@end

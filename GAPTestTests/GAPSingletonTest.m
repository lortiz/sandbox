//
//  GAPSingletonTest.m
//  GAPTest
//
//  Created by Leonardo Ortiz on 4/1/14.
//  Copyright (c) 2014 Redstone. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "GAPSingleton.h"

@interface GAPSingletonTest : XCTestCase

@end

@implementation GAPSingletonTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testGapSingletonWrite{
    GAPSingleton *singleton = [GAPSingleton sharedInstance];
    singleton.userInput = @"This is not a null string";
    XCTAssertEqual(singleton.userInput,@"This is not a null string", @"This variable must have a string");
}

-(void)testGapSingletonInitialization{
    GAPSingleton *singleton = [GAPSingleton sharedInstance];
    XCTAssertNotNil(singleton, @"This singleton can not be nil");
}

@end
